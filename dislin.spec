#
# spefile for dislin
#
Name: %{name} 
Version: %{version} 
Release: %{release} 
Packager: %{packager}
Summary: Data plotting library DISLIN for Linux
License: free
Group: Applications/Graphics
URL: https://www.dislin.de
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot 
Prefix: %{_prefix}

%description
DISLIN is a high-level and easy to use plotting library for
displaying data as curves, bar graphs, pie charts, 3D-colour plots,
surfaces, contours and maps. Several output formats are supported
such as X11, VGA, PostScript, PDF, CGM, SVG, HPGL, TIFF, GIF and PNG.
The library contains about 600 plotting and parameter setting routines
and is available for several C, Fortran 77 and Fortran 90/95 compilers.


%prep

%build

%install 

# copy includes to RPM_BUILD_ROOT and set aliases
mkdir -p $RPM_BUILD_ROOT%{_prefix}
cp -rp %{sources_dir}/* $RPM_BUILD_ROOT%{_prefix}/.

#Change access rights
chmod -R 755 $RPM_BUILD_ROOT%{_prefix}/lib

%clean 

%post 

%postun 

%files 
%defattr(-, root, root) 
%{_prefix}/*




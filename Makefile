SHELL := /bin/bash
RPMBUILD_DIR=${PWD}
PackageName=dislin
PACKAGE_VER=11.5
PACKAGE_RELEASE=1
Packager = Daniel Gastler

PLATFORM := $(shell uname -p)
#PLATFORM=aarch64
DISTRO_NAME=$(shell lsb_release -i -s)
DISTRO_RELEASE=$(shell lsb_release -r -s | awk '{print int($$1)}')

RPM_OUTPUT_DIR=${RPMBUILD_DIR}/rpm/rpm/${DISTRO_NAME}/${DISTRO_RELEASE}/${PLATFORM}/

DOWNLOAD_DISTRO_aarch64=arm_64
DOWNLOAD_DISTRO_armv7l=arm

DOWNLOAD_PLATFORM=$(DOWNLOAD_DISTRO_$(PLATFORM))

DOWNLOAD_URL=https://www.dislin.de/downloads/linux/${DOWNLOAD_PLATFORM}/
DOWNLOAD_FILENAME=dislin-${PACKAGE_VER}.linux.${DOWNLOAD_PLATFORM}.tar.gz
DOWNLOAD_FILE=${DOWNLOAD_URL}${DOWNLOAD_FILENAME}

SRC_DIR=${PackageName}-${PACKAGE_VER}

${DOWNLOAD_FILENAME}: 
	wget ${DOWNLOAD_FILE}

${SRC_DIR}: ${DOWNLOAD_FILENAME}
	tar -xf ${DOWNLOAD_FILENAME}

rpm: _rpmall
_rpmall: ${SRC_DIR}
	# create the folder structure
	mkdir -p ${RPMBUILD_DIR}/{RPMS/${PLATFORM},SPECS,BUILD,SOURCES,SRPMS}

	# "install" the files from the tarball
	install -m 755 -d ${RPMBUILD_DIR}/SOURCES/include/real64
	install -m 644 ${SRC_DIR}/license.txt         ${RPMBUILD_DIR}/SOURCES/include/
	install -m 644 ${SRC_DIR}/examples/dislin.h   ${RPMBUILD_DIR}/SOURCES/include/
	install -m 644 ${SRC_DIR}/examples/dislin_d.h ${RPMBUILD_DIR}/SOURCES/include/real64/dislin.h
	install -m 644 ${SRC_DIR}/examples/discpp.h   ${RPMBUILD_DIR}/SOURCES/include/

	install -m 755 -d ${RPMBUILD_DIR}/SOURCES/lib/
	install -m 644 ${SRC_DIR}/license.txt         ${RPMBUILD_DIR}/SOURCES/lib/
	install -m 644 ${SRC_DIR}/linux_${DOWNLOAD_PLATFORM}/lib/*.so   ${RPMBUILD_DIR}/SOURCES/lib/


	ln -s -f dislin-${PACKAGE_VER}.so ${RPMBUILD_DIR}/SOURCES/lib/libdislin.so.$(firstword $(subst ., ,${PACKAGE_VER}))
	ln -s -f libdislin.so.$(firstword $(subst ., ,${PACKAGE_VER})) ${RPMBUILD_DIR}/SOURCES/lib/libdislin.so

	ln -s -f dislin_d-${PACKAGE_VER}.so ${RPMBUILD_DIR}/SOURCES/lib/libdislin_d.so.$(firstword $(subst ., ,${PACKAGE_VER}))
	ln -s -f libdislin_d.so.$(firstword $(subst ., ,${PACKAGE_VER})) ${RPMBUILD_DIR}/SOURCES/lib/libdislin_d.so

	ln -s -f discpp-${PACKAGE_VER}.so ${RPMBUILD_DIR}/SOURCES/lib/libdiscpp.so.$(firstword $(subst ., ,${PACKAGE_VER}))
	ln -s -f libdiscpp.so.$(firstword $(subst ., ,${PACKAGE_VER})) ${RPMBUILD_DIR}/SOURCES/lib/libdiscpp.so




	# build the rpm
	rpmbuild -bb -bl --buildroot=${RPMBUILD_DIR}/BUILD						\
			--define "_topdir ${RPMBUILD_DIR}"						\
			--define "_prefix /opt/dislin"							\
			--define "sources_dir ${RPMBUILD_DIR}/SOURCES"					\
			--define "name ${PackageName}"								\
			--define "version ${PACKAGE_VER}"	\
			--define "release ${PACKAGE_RELEASE}"							\
			--define "packager ${Packager}"								\
			${PackageName}.spec
	mkdir -p ${RPM_OUTPUT_DIR}
	find ${RPMBUILD_DIR}/RPMS -name "*.rpm" -exec mv {} ${RPM_OUTPUT_DIR} \;

clean:
	rm -rf {RPMS,SPECS,BUILD,SOURCES,SRPMS}
	rm -rf BUILDROOT RPMS
	rm -rf ./${SRC_DIR}
	rm -f ${DOWNLOAD_FILENAME}

clean_rpm: clean
	rm -rf rpm

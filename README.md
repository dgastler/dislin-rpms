This repo is for building RPMS for the dislin plotting package [link](https://www.dislin.de/).
If you use this repo to build RPMs, please make sure you follow the licensing of the DISLIN package.
